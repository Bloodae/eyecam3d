#include "stdafx.h"
#include "MyRTSPServer.h"

#include <liveMedia.hh>
#include <BasicUsageEnvironment.hh>
#include <GroupsockHelper.hh>


MyRTSPServer::MyRTSPServer()
{
	const char *inputFileName = "DummyFileName";

	// Begin by setting up our usage environment:
	TaskScheduler* scheduler = BasicTaskScheduler::createNew();
	UsageEnvironment* env = BasicUsageEnvironment::createNew(*scheduler);

	// Create 'groupsocks' for RTP and RTCP:
	struct in_addr destinationAddress;
	destinationAddress.s_addr = chooseRandomIPv4SSMAddress(*env);

	const unsigned short rtpPortNum = 18888;
	const unsigned short rtcpPortNum = rtpPortNum + 1;
	const unsigned char ttl = 255;

	const Port rtpPort(rtpPortNum);
	const Port rtcpPort(rtcpPortNum);

	Groupsock rtpGroupsock(*env, destinationAddress, rtpPort, ttl);
	rtpGroupsock.multicastSendOnly(); // we're a SSM source
	Groupsock rtcpGroupsock(*env, destinationAddress, rtcpPort, ttl);
	rtcpGroupsock.multicastSendOnly(); // we're a SSM source

									   // Create a 'JPEG Video RTP' sink from the RTP 'groupsock':
	RTPSink* videoSink = JPEGVideoRTPSink::createNew(*env, &rtpGroupsock);

	// Create (and start) a 'RTCP instance' for this RTP sink:
	const unsigned estimatedSessionBandwidth = 256000; // in kbps; for RTCP b/w share
	const unsigned maxCNAMElen = 100;
	unsigned char CNAME[maxCNAMElen + 1];
	gethostname((char*)CNAME, maxCNAMElen);
	CNAME[maxCNAMElen] = '\0'; // just in case
	RTCPInstance* rtcp = RTCPInstance::createNew(*env, &rtcpGroupsock,
		estimatedSessionBandwidth, CNAME,
		videoSink, NULL /* we're a server */,
		True /* we're a SSM source */);
	// Note: This starts RTCP running automatically

	RTSPServer* rtspServer = RTSPServer::createNew(*env, 8554);
	if (rtspServer == NULL) {
		*env << "Failed to create RTSP server: " << env->getResultMsg() << "\n";
		exit(1);
	}
	ServerMediaSession* sms = ServerMediaSession::createNew(*env, "testStream", inputFileName, "Session streamed by \"testMJPEGVideoStreamer\"",
		True /*SSM*/);
	sms->addSubsession(PassiveServerMediaSubsession::createNew(*videoSink, rtcp));
	rtspServer->addServerMediaSession(sms);

	char* url = rtspServer->rtspURL(sms);
	*env << "Play this stream using the URL \"" << url << "\"\n";
	delete[] url;

	// Start the streaming:
	*env << "Beginning streaming...\n";
	// Open the input file as a 'byte-stream file source':
	ByteStreamMemoryBufferSource* bufferSource = ByteStreamMemoryBufferSource::createNew(*env, jpegBuffer, jpegBufferSize, false);
	if (bufferSource == NULL) {
		*env << "Unable to open file \"" << inputFileName
			<< "\" as a byte-stream file source\n";
		exit(1);
	}

	// Create the MJPEG video source:
	videoSource = MJPEGVideoSource::createNew(*env, bufferSource);

	// Finally, start playing:
	*env << "Beginning to read from file...\n";
	videoSink->startPlaying(*videoSource, NULL, NULL);

	env->taskScheduler().doEventLoop(&(videoSource->watchVariable));

	/*return 0;*/
}


MyRTSPServer::~MyRTSPServer()
{
}

void MyRTSPServer::SetNewBuffer(u_int8_t * bufptr, u_int64_t bufSize)
{
	jpegBuffer = bufptr;
	jpegBufferSize = bufSize;
	while (videoSource->watchVariable) {
		Sleep(1);
	}
	videoSource->watchVariable = 1;
}
