#include "stdafx.h"
#include "DualPylonCameraGrabber.h"


// Namespace for using pylon objects.
using namespace Pylon;
// Namespace for using GenApi objects.
using namespace GenApi;
// Namespace for using cout.
using namespace std;

// Limits the amount of cameras used for grabbing.
// It is important to manage the available bandwidth when grabbing with multiple cameras.
static const size_t c_maxCamerasToUse = 2;

//Assumes that both cameras are identical
DualPylonCameraGrabber::DualPylonCameraGrabber()
{
	camerasInitialized = InitializeCameras();

	if (camerasInitialized)
	{
		//Set the size of the SBS image buffer. Rolling 2x buffer
		INodeMap& nodemap = (*cameras)[0].GetNodeMap();
		CIntegerPtr width(nodemap.GetNode("Width"));
		CIntegerPtr height(nodemap.GetNode("Height"));
		framePairRollingBuffer = shared_ptr<uint8_t>(new uint8_t[2 * bufferFrameCount * 3 * (uint32_t)width->GetMax() * (uint32_t)height->GetMax()]);
	}

}


DualPylonCameraGrabber::~DualPylonCameraGrabber()
{
}


bool DualPylonCameraGrabber::InitializeCameras()
{
	bool exitCode = false;

	// Before using any pylon methods, the pylon runtime must be initialized. 
	PylonInitialize();

	try
	{
		// Get the transport layer factory.
		CTlFactory& tlFactory = CTlFactory::GetInstance();

		// Get all attached devices and exit application if no device is found.
		DeviceInfoList_t devices;
		uint32_t deviceCount = tlFactory.EnumerateDevices(devices);
		if (deviceCount == 0)
		{
			throw RUNTIME_EXCEPTION("No camera present.");
		}
		else if (deviceCount == 1)
		{
			throw RUNTIME_EXCEPTION("One camera is missing.");
		}
		else if (deviceCount > 2)
		{
			throw RUNTIME_EXCEPTION("More than two cameras are connected.");
		}

		// Create an array of instant cameras for the found devices and avoid exceeding a maximum number of devices.
		cameras = unique_ptr<CInstantCameraArray>(new CInstantCameraArray(min(devices.size(), c_maxCamerasToUse)));

		// Create and attach all Pylon Devices.
		for (size_t i = 0; i < (*cameras).GetSize(); ++i)
		{
			(*cameras)[i].Close();
			(*cameras)[i].Attach(tlFactory.CreateDevice(devices[i]));
			(*cameras)[i].Open();
		}

		if (ResetCameras())
		{
			exitCode = true;
		}
	}
	catch (const GenericException &e)
	{
		// Error handling
		cerr << "An exception occurred." << endl
			<< e.GetDescription() << endl;
	}

	return exitCode;
}


//Changes the position of the left and right cameras in the array.
void DualPylonCameraGrabber::ReverseCameras() {
	reverseCameras = (++reverseCameras) % 2;
}

void DualPylonCameraGrabber::GrabSBSImage()
{
	//Change bufferIterator at the end of writing. So, if value == 1, write at position 0. If value  == 0, write at 1.
	cameras->StartGrabbing();

	// Grab c_countOfImagesToGrab from the cameras.
	for (int i = 0; i < 2 && cameras->IsGrabbing(); ++i)
	{
		cameras->RetrieveResult(frameDelay_ms - 1, ptrGrabResult, TimeoutHandling_ThrowException);
		if (ptrGrabResult->GrabSucceeded())
		{
			unsigned int sourcePixelIndex = 0;
			unsigned int pixelStep = 1;
			unsigned int byteToWrite = 2 * ((bufferIterator + 1) % bufferFrameCount) + (reverseCameras + ptrGrabResult->GetCameraContext()) % 2;
			byteToWrite *= bytesPerPixel * ptrGrabResult->GetWidth() * ptrGrabResult->GetHeight();
			const uint8_t *imageBuffer = (uint8_t *)ptrGrabResult->GetBuffer();

			if (useHalfSBS)
			{
				pixelStep = 2;	//Skip hald the pixels, horizontally
				byteToWrite /= 2;	//Reduce image size by 2, therefore buffer size needed (note:memory allocation doesn't change)
			}

			for (unsigned int j = 0; j < ptrGrabResult->GetHeight(); ++j, byteToWrite += bytesPerPixel * ptrGrabResult->GetWidth() / pixelStep)
			{
				for (unsigned int k = 0; k < bytesPerPixel * ptrGrabResult->GetWidth() / pixelStep; ++k, sourcePixelIndex += pixelStep, ++byteToWrite)
				{
					framePairRollingBuffer.get()[byteToWrite] = imageBuffer[sourcePixelIndex];
				}
			}

		}
		else {
			throw RUNTIME_EXCEPTION("An exception occured when grabbing a frame from the cameras.");
		}

	}

	latestFrameWidth = ptrGrabResult->GetWidth();
	latestFrameHeight = ptrGrabResult->GetHeight();
	bufferIterator = (++bufferIterator) % bufferFrameCount;
}

bool DualPylonCameraGrabber::ResetCameras()
{
	bool exitCode = false;

	if (camerasInitialized)
	{
		try
		{
			for (size_t i = 0; i < (*cameras).GetSize(); ++i)
			{
				//Reset cameras to default parameters
				(*cameras)[i].Open();
				(*cameras)[i].RegisterConfiguration(new CSoftwareTriggerConfiguration(), ERegistrationMode::RegistrationMode_ReplaceAll, ECleanup::Cleanup_Delete);
				INodeMap& nodemap = (*cameras)[i].GetNodeMap();

				CEnumerationPtr pixelFormat(nodemap.GetNode("PixelFormat"));
				if (IsAvailable(pixelFormat->GetEntryByName("RGB8")))
				{
					pixelFormat->FromString("RGB8");
				}
				else
				{
					throw RUNTIME_EXCEPTION("RGB 8 pixel format is not supported.");
				}

				CEnumerationPtr gainAuto(nodemap.GetNode("GainAuto"));
				if (IsWritable(gainAuto))
				{
					gainAuto->FromString("On");
				}
			}

			if (ResizeImages())
			{
				exitCode = true;
			}
		}
		catch (const GenericException &e)
		{
			// Error handling
			cerr << "An exception occurred." << endl
				<< e.GetDescription() << endl;
		}
	}

	return exitCode;
}

bool DualPylonCameraGrabber::ResizeImages(int64_t newWidth, int64_t newHeight)
{
	bool exitCode = false;

	if (camerasInitialized)
	{
		try
		{
			for (size_t i = 0; i < (*cameras).GetSize(); ++i)
			{
				//Reset cameras to default parameters
				(*cameras)[i].Open();
				INodeMap& nodemap = (*cameras)[i].GetNodeMap();
				CIntegerPtr offsetX(nodemap.GetNode("OffsetX"));
				CIntegerPtr offsetY(nodemap.GetNode("OffsetY"));
				CIntegerPtr width(nodemap.GetNode("Width"));
				CIntegerPtr height(nodemap.GetNode("Height"));

				if (newWidth == 0 && newHeight == 0)
				{
					offsetX->SetValue(offsetX->GetMin());
					offsetY->SetValue(offsetY->GetMin());
					width->SetValue(width->GetMax());
					height->SetValue(height->GetMax());
				}
				else {
					newWidth = Adjust(newWidth, width->GetMin(), width->GetMax(), width->GetInc());
					newHeight = Adjust(newHeight, height->GetMin(), height->GetMax(), height->GetInc());
					uint64_t newOffsetX = Adjust((width->GetMax() - newWidth) / 2, offsetX->GetMin(), offsetX->GetMax(), offsetX->GetInc());
					uint64_t newOffsetY = Adjust((height->GetMax() - newHeight) / 2, offsetY->GetMin(), offsetY->GetMax(), offsetY->GetInc());

					offsetX->SetValue(newOffsetX);
					offsetY->SetValue(newOffsetY);
					width->SetValue(newWidth);
					height->SetValue(newHeight);
				}
			}

			exitCode = true;
		}
		catch (const GenericException &e)
		{
			// Error handling
			cerr << "An exception occurred." << endl
				<< e.GetDescription() << endl;
		}
	}

	return exitCode;
}

const uint8_t * DualPylonCameraGrabber::GetLatestSBSFrame()
{
	int imagePosition = bufferIterator * bytesPerPixel * latestFrameWidth * latestFrameHeight;

	if (!useHalfSBS) {
		imagePosition *= 2;
	}


	return framePairRollingBuffer.get() + imagePosition;
}


// Adjust value to make it comply with range and increment passed.
//
// The parameter's minimum and maximum are always considered as valid values.
// If the increment is larger than one, the returned value will be: min + (n * inc).
// If the value doesn't meet these criteria, it will be rounded down to ensure compliance.
int64_t DualPylonCameraGrabber::Adjust(int64_t val, int64_t minimum, int64_t maximum, int64_t inc)
{
	// Check the input parameters.
	if (inc <= 0)
	{
		// Negative increments are invalid.
		throw LOGICAL_ERROR_EXCEPTION("Unexpected increment %d", inc);
	}
	if (minimum > maximum)
	{
		// Minimum must not be bigger than or equal to the maximum.
		throw LOGICAL_ERROR_EXCEPTION("minimum bigger than maximum.");
	}

	// Check the lower bound.
	if (val < minimum)
	{
		return minimum;
	}

	// Check the upper bound.
	if (val > maximum)
	{
		return maximum;
	}

	// Check the increment.
	if (inc == 1)
	{
		// Special case: all values are valid.
		return val;
	}
	else
	{
		// The value must be min + (n * inc).
		// Due to the integer division, the value will be rounded down.
		return minimum + (((val - minimum) / inc) * inc);
	}
}
