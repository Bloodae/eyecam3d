#pragma once


class JPEGHandler
{
public:

	struct ImageInfo
	{
		unsigned char * bytes = nullptr;
		int width = 0;
		int height = 0;
		int bytesCount = 0;
	};




	JPEGHandler();
	~JPEGHandler();
	void RGB2JPEG(JPEGHandler::ImageInfo rgbImage, int quality);
	bool JPEGFile2RGB(const char filename[]);
	void Write2File(const char filename[], JPEGHandler::ImageInfo imageInfo);
	JPEGHandler::ImageInfo getRGBImage();
	JPEGHandler::ImageInfo getJPEGImage();

private:
	long unsigned int biggestJPEGSize = 0;
	ImageInfo JPEGImage;
	ImageInfo RGBImage;
};

